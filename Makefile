export PATH := ./node_modules/.bin:$(PATH)

	
all: dist/icon.png dist/favicon.ico dist/favicon.svg dist/100x100/gachou-logo.png dist/200x200/gachou-logo.png dist/100x100/gachou-logo-5-errors.png dist/gachou-logo.svg

dist: 
	mkdir -p dist/200x200 dist/100x100

dist/favicon.ico: svg/gachou-logo-2021.svg dist Makefile
	convert -background none svg/gachou-logo-2021.svg -resize 64x64 dist/favicon.ico

dist/icon.png: svg/gachou-logo-2021.svg dist Makefile
	convert -background none svg/gachou-logo-2021.svg dist/icon.png
	

dist/200x200/gachou-logo.png: svg/gachou-logo.svg dist Makefile
	inkscape $< --export-png=$@ 
	
dist/100x100/gachou-logo.png: svg/gachou-logo.svg dist Makefile
	inkscape $< --export-png=$@ --export-height=100 --export-width=100 


dist/gachou-logo.svg: svg/gachou-logo.svg dist Makefile
	svgo --input=$< --output=$@

dist/favicon.svg: svg/gachou-logo-2021.svg dist Makefile
	svgo --input=$< --output=$@


